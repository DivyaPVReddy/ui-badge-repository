import { CheckboxWithLabel, Fonts, Inset } from 'hss_components';
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { IDispatchProp } from 'types/dispatch-prop';
import IGlobalState from 'types/global-state';
import { SpecifyType } from 'types/rules-engine';
import { setRulesFilter, updateDuplicateList } from '../act.rules-engine';
import RuleCustomerSelect from './components/rule-customer-select';
import DuplicateNotice from './components/rule-duplicate-notice';
import { RuleUserSelect } from './components/rule-user-select';
const TopContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const FilterContainer = styled.div`
  background-color: white;
  margin: 5px 0px;
  padding: 25px;
  display: block;
`;

const StyledCommonTitle = styled(Fonts.Display20)`
  margin-bottom: 4px;
  white-space: nowrap;
`;

const FilterCheckBoxContainer = styled(Fonts.Body12)`
  color: ${props => props.theme.colors.grey5};
  display: block;
  flex-direction: column;
  margin-left: 8px;
`;

const StyledFilterCheckboxWithLabel = styled(CheckboxWithLabel)`
  margin-right: 25px;
  margin-bottom: 10px;
  margin-top: 20px;
  margin-left: 10px;
`;

const InputContainer = styled.div`
  background-color: ${props => props.theme.colors.grey2};
  width: 430px;
  padding: 5px;
  display: block;
  margin-top: 20px;
`;
const DuplicateContainer = styled(Inset)`
  box-sizing: border-box;
  height: 40px;
  width: 1240px;
  border: 1px solid ${props => props.theme.colors.red};
  border-radius: 2px;
  background-color: #ffb6c147;
  padding: 12px;
  color: ${props => props.theme.colors.black};
  display: flex;
`;
interface IStateProps {
  /***
   * Option checked
   */
  checked?: string[];
  /**
   *  duplicate check for customer
   */
  duplicateCust?: any[];
  /***
   * duplicate check for user
   */
  duplicateUser?: any[];
}

type IProps = IStateProps & IDispatchProp;
const RuleFilter: React.SFC<IProps> = props => {
  const handleChecked = (filter: string) => {
    props.dispatch(setRulesFilter(filter));
  };

  const handleCustomerClose = () => {
    /**  */
    props.dispatch(updateDuplicateList(SpecifyType.customers));
  };

  const handleUserClose = () => {
    props.dispatch(updateDuplicateList(SpecifyType.users));
  };
  /** export customer checkbox */
  const renderCustomerCheckbox = () => {
    return (
      <StyledFilterCheckboxWithLabel
        value={'Customers'}
        onChange={handleChecked}
        checked={props.checked.includes('Customers')}
      >
        <Fonts.Body14>Customers</Fonts.Body14>
      </StyledFilterCheckboxWithLabel>
    );
  };

  const renderUserCheckbox = () => {
    return (
      <StyledFilterCheckboxWithLabel
        value={'Users'}
        onChange={handleChecked}
        checked={props.checked.includes('Users')}
      >
        Users
      </StyledFilterCheckboxWithLabel>
    );
  };
  return (
    <FilterContainer>
      <TopContainer>
        <StyledCommonTitle>3. Specify Customers/ Users</StyledCommonTitle>
      </TopContainer>
      {props.duplicateCust[0] &&
        props.duplicateCust[0].length && (
          <DuplicateContainer>
            <DuplicateNotice
              duplicate={props.duplicateCust[0]}
              option={'customer'}
              closeFn={handleCustomerClose}
            />
          </DuplicateContainer>
        )}
      {props.duplicateUser[0] &&
        props.duplicateUser[0].length > 0 && (
          <DuplicateContainer>
            <DuplicateNotice
              duplicate={props.duplicateUser[0]}
              option={'user'}
              closeFn={handleUserClose}
            />
          </DuplicateContainer>
        )}
      <FilterCheckBoxContainer>
        {props.checked.includes('Customers') ? (
          <InputContainer>
            {renderCustomerCheckbox()}
            <RuleCustomerSelect />
          </InputContainer>
        ) : (
          renderCustomerCheckbox()
        )}
        {props.checked.includes('Users') ? (
          <InputContainer>
            {renderUserCheckbox()}
            <RuleUserSelect />
          </InputContainer>
        ) : (
          renderUserCheckbox()
        )}
      </FilterCheckBoxContainer>
    </FilterContainer>
  );
};

const mapStateToProps = (state: IGlobalState): IProps => {
  return {
    checked: state.rulesEngine.selectedFilter,
    duplicateCust: state.rulesEngine.duplicateCustEntry,
    duplicateUser: state.rulesEngine.duplicateUserEntry,
  };
};

/** rule filter - customer/user */
export default connect<IDispatchProp, IProps>(mapStateToProps)(RuleFilter);
