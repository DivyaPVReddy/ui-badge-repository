// tslint:disable:max-file-line-count
import { addDays, format } from 'date-fns';
import {
  Fonts,
  FormTextArea,
  Inset,
  SingleDatePicker,
  TextField,
} from 'hss_components';
import Router from 'next/router';
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { IDispatchProp } from 'types/dispatch-prop';
import IGlobalState from 'types/global-state';
import { setNameFields, setRuleRequestDate } from '../act.rules-engine';

const TopContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;
const NameContainer = styled.div`
  background-color: white;
  margin: 5px 0px;
  height: 357px;
  padding: 25px;
`;

const StyledCommonTitle = styled(Fonts.Display20)`
  margin-bottom: 4px;
  white-space: nowrap;
`;

const DropDownWraper = styled.div`
  display: flex;
  width: 40%;
`;

const DatePickerContainer = styled.div`
  margin-right: 10px;
`;

const StyledText = styled(TextField)`
  textarea {
    width: 40%;
    height: 26px;
    border-radius: 0;
    resize: none;
    padding: 0;
    font-size: 14px;
    margin: 4px;
    line-height: 26px;
    &:focus {
      /* border: none; */
    }
  }
  & div[class*='ase-input-field'] {
    color: ${props => props.theme.colors.black};
    font-family: 'Work Sans';
    font-weight: 600;
  }
`;

const StyledComponentWithLabel = styled(Inset)`
  background-color: ${props => props.theme.colors.white};
  width: 60%;
  div[class*='component-with-label'] {
    margin-bottom: 4px;
  }
`;
const StyledFormTextAreaDescription = styled(FormTextArea)`
  textarea {
    width: 100%;
    height: 26px;
    border-radius: 0;
    resize: none;
    padding: 0;
    font-size: 14px;
    margin: 4px;
    line-height: 26px;
    &:focus {
      /* border: none; */
    }
    &::placeholder {
      color: ${props => props.theme.colors.grey3};
    }
  }
`;

const StyledFormTextAreaComments = styled(FormTextArea)`
  textarea {
    width: 40%;
    height: 125px;
    border-radius: 0;
    resize: none;
    padding: 0;
    font-size: 14px;
    margin: 4px;
    line-height: 26px;
    &:focus {
      /* border: none; */
    }
    &::placeholder {
      color: ${props => props.theme.colors.grey3};
    }
  }
`;

const InputHeader = styled.div`
  justify-content: space-between;
  display: flex;
  width: 405px;
  margin-top: 17px;
  margin-bottom: 4px;
`;

const InputHeaderDropDown = styled.div`
  justify-content: space-between;
  display: flex;
  width: 170px;
  margin-top: 17px;
  margin-bottom: 4px;
`;

const StyledDateRangePicker = styled(SingleDatePicker)`
  width: 100%;
  height: 28px;
  margin-top: 8px;
  border-color: ${props => props.theme.colors.grey3};
`;

const HeaderText = styled(Fonts.makeLight(Fonts.Body12))``;
interface IStateProps {
  /**
   * list of action type and Ids
   */
  date?: string;
  /**
   * if the rule name is duplicate
   */
  uniqueName?: boolean;
  /**
   *  rule name , desct n requested by
   */
  name?: string;
  descpt?: string;
  reqstBy?: string;
}
type IProps = IStateProps & IDispatchProp;
class UnconnectedRuleName extends React.Component<IProps> {
  onEdit = (event: any) => {
    this.props.dispatch(setNameFields('name', event));
  };

  onEditComments = (event: any) => {
    this.props.dispatch(setNameFields('descpt', event.currentTarget.value));
  };

  onEditBy = (event: any) => {
    this.props.dispatch(setNameFields('requestBy', event.currentTarget.value));
  };

  setRuleRequest = (date: Date) => {
    this.props.dispatch(setRuleRequestDate(format(date, 'MM/DD/YYYY')));
  };

  backButtonCallback = () => {
    Router.push('/rules-engine');
  };

  getTitle = (date: string) =>
    date ? `${format(date, 'MM/DD/YYYY')}` : 'mm/dd/yyyy';
  render() {
    return (
      <NameContainer>
        <TopContainer>
          <StyledCommonTitle>1. Name the rule</StyledCommonTitle>
          <HeaderText>
            <sup>*</sup> All fields are required
          </HeaderText>
        </TopContainer>
        <InputHeader>
          <Fonts.Bold14>Name</Fonts.Bold14>
        </InputHeader>
        <StyledComponentWithLabel>
          <StyledText
            placeholder=""
            id={`name`}
            width={402}
            value={this.props.name.substr(0, 100)}
            onChange={this.onEdit}
            valid={this.props.uniqueName}
            errorMessage="Duplicate Rule Name"
          />
        </StyledComponentWithLabel>
        <InputHeader>
          <Fonts.Bold14>Description</Fonts.Bold14>
        </InputHeader>
        <StyledFormTextAreaComments
          placeholder=""
          name="description"
          value={this.props.descpt.substr(0, 1000)}
          onChange={this.onEditComments}
        />
        <DropDownWraper>
          <DatePickerContainer>
            <InputHeaderDropDown>
              <Fonts.Bold14>Requested by</Fonts.Bold14>
            </InputHeaderDropDown>
            <StyledFormTextAreaDescription
              placeholder=""
              name="requestedBy"
              value={this.props.reqstBy.substr(0, 40)}
              onChange={this.onEditBy}
            />
          </DatePickerContainer>
          <DatePickerContainer>
            <InputHeaderDropDown>
              <Fonts.Bold14>Requested Date</Fonts.Bold14>
            </InputHeaderDropDown>
            <StyledDateRangePicker
              defaultDate={new Date()}
              title={this.getTitle(this.props.date)}
              hardFromDate={addDays(new Date(), 0)}
              hardToDate={addDays(new Date(), 180)}
              setDate={this.setRuleRequest}
              platform={'CSR'}
            />
          </DatePickerContainer>
        </DropDownWraper>
      </NameContainer>
    );
  }
}

const mapStateToProps = (state: IGlobalState): IProps => {
  return {
    date: state.rulesEngine.requestDate,
    uniqueName: state.rulesEngine.duplicateName,
    name: state.rulesEngine.ruleName,
    descpt: state.rulesEngine.description,
    reqstBy: state.rulesEngine.requestedby,
  };
};
/*** Name section of create rules engine */
export const RuleName = connect<IDispatchProp, IProps>(mapStateToProps)(
  UnconnectedRuleName,
);
