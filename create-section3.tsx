// tslint:disable:max-file-line-count
import { renderBusinessIndicator } from 'components/indicators/business-unit';
import { addYears, format, startOfDay } from 'date-fns';
import {
  CheckboxWithLabel,
  DateRangePicker,
  Fonts,
  RadioButton,
} from 'hss_components';
import Router from 'next/router';
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { IDispatchProp } from 'types/dispatch-prop';
import { IDynamicDate } from 'types/dynamic-date';
import IGlobalState from 'types/global-state';
import { IBuUnit } from 'types/rules-engine';
import {
  setEffectiveDateForRule,
  setNameFields,
  setSelectedBuUnit,
} from '../act.rules-engine';
import { ActionTypeFilter } from './components/rule-action-types';

const TopContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const DefineContainer = styled.div`
  background-color: white;
  margin: 5px 0px;
  min-height: 270px;
  padding: 25px;
`;

const StyledCommonTitle = styled(Fonts.Display20)`
  margin-bottom: 4px;
  white-space: nowrap;
`;

const DatePickerContainer = styled.div`
  margin-right: 10px;
`;

const InputHeader = styled.div`
  justify-content: space-between;
  display: flex;
  width: 405px;
  margin-top: 17px;
  margin-bottom: 4px;
`;

const InputHeaderDropDown = styled.div`
  justify-content: space-between;
  display: flex;
  width: 170px;
  margin-top: 17px;
  margin-bottom: 4px;
`;

const RadioGroup = styled.div`
  display: flex;
  align-items: center;
  align-content: center;
`;

const RadioWithMarginRight = styled(RadioButton)`
  margin-right: 20px;
`;

const RadioLabel = styled(Fonts.Body12)`
  align-self: center;
`;

const CheckBoxContainer = styled(Fonts.Body12)`
  color: ${props => props.theme.colors.grey5};
  display: flex;
  margin-left: 8px;
`;

const StyledCheckboxWithLabel = styled(CheckboxWithLabel)`
  margin-right: 25px;
`;

const StyledEffectiveDateRangePicker = styled(DateRangePicker)`
  width: 20%;
  height: 28px;
  margin-top: 8px;
  border-color: ${props => props.theme.colors.grey3};
  & div[class*='date-picker-dropdown__StyledDatePickerWrapper'] {
    right: auto;
  }
`;

const HeaderText = styled(Fonts.makeLight(Fonts.Body12))``;

interface IStateProps {
  /**
   * Effective date
   */
  dates?: IDynamicDate;
  /***
   *
   */
  buUnit?: string[];
  /***
   *
   */
  units?: IBuUnit[];
}

interface IState {
  isSelected: string;
  isChecked: string[];
  dateSet: boolean;
}
type IProps = IDispatchProp & IStateProps;
class RuleDefine extends React.Component<IProps> {
  state: IState = {
    isSelected: 'inactive',
    isChecked: ['ONC'],
    dateSet: false,
  };

  formatDate = (inputDate: Date) => {
    return format(inputDate, 'MM/DD/YY');
  };

  dateRangeString = () => {
    return this.props.dates.title === ''
      ? `${format(this.props.dates.fromDate, 'MM/DD/YYYY')} - ${format(
          this.props.dates.toDate,
          'MM/DD/YYYY',
        )}`
      : 'mm/dd/yyyy - mm/dd/yyyy';
  };

  backButtonCallback = () => {
    Router.push('/rules-engine');
  };
  handleChange = (event: any) => {
    this.setState({ isSelected: event.currentTarget.value });
    this.props.dispatch(setNameFields('status', event.currentTarget.value));
  };

  handleCheck = (event: any) => {
    this.props.dispatch(setSelectedBuUnit(event));
  };
  setRulesEffectiveDate = (date: IDynamicDate) => {
    this.setState({ dateSet: true });
    this.props.dispatch(setEffectiveDateForRule(date));
  };

  renderCheckbox = () => {
    const checkBox = this.props.units.map((item: IBuUnit, idx: number) => {
      return (
        <StyledCheckboxWithLabel
          key={idx}
          value={item.BusUnitCode}
          onChange={this.handleCheck}
          checked={this.props.buUnit.includes(item.BusUnitCode)}
        >
          {renderBusinessIndicator(item.BusUnitCode, item.BusUnitDesc)}
        </StyledCheckboxWithLabel>
      );
    });
    return checkBox;
  };

  render() {
    const today = new Date();
    const startOfToday = startOfDay(today);
    return (
      <DefineContainer>
        <TopContainer>
          <StyledCommonTitle>2. Define the Rule</StyledCommonTitle>
          <HeaderText>
            <sup>*</sup> All fields are required
          </HeaderText>
        </TopContainer>
        <InputHeader>
          <Fonts.Bold14>Status</Fonts.Bold14>
        </InputHeader>
        <RadioGroup>
          <RadioWithMarginRight
            name={'active'}
            value={'active'}
            checked={this.state.isSelected === 'active'}
            onChange={this.handleChange}
          >
            <RadioLabel>Active</RadioLabel>
          </RadioWithMarginRight>
          <RadioWithMarginRight
            name={'inactive'}
            value={'inactive'}
            checked={this.state.isSelected === 'inactive'}
            onChange={this.handleChange}
          >
            <RadioLabel>Inactive</RadioLabel>
          </RadioWithMarginRight>
          <RadioButton
            name={'archived'}
            value={'archived'}
            checked={this.state.isSelected === 'archived'}
            onChange={this.handleChange}
          >
            <RadioLabel>Archived</RadioLabel>
          </RadioButton>
        </RadioGroup>

        <InputHeader>
          <Fonts.Bold14>Business Unit</Fonts.Bold14>
        </InputHeader>
        <CheckBoxContainer>{this.renderCheckbox()}</CheckBoxContainer>
        <DatePickerContainer>
          <InputHeaderDropDown>
            <Fonts.Bold14>Effective Date</Fonts.Bold14>
          </InputHeaderDropDown>
          <StyledEffectiveDateRangePicker
            defaultDates={{
              title: 'Yesterday',
              fromDate: today,
              toDate: addYears(startOfToday, 1),
            }}
            title={
              this.props.dates ? this.dateRangeString() : 'mm/dd/yyyy - mm/dd/yyyy'
            }
            setDates={this.setRulesEffectiveDate}
            hardFromDate={new Date()}
            hardToDate={addYears(startOfToday, 7979)}
          />
        </DatePickerContainer>
        <ActionTypeFilter />
      </DefineContainer>
    );
  }
}

const mapStateToProps = (state: IGlobalState): IProps => {
  return {
    dates: state.rulesEngine.effectiveDate,
    buUnit: state.rulesEngine.selectedBuUnits,
    units: state.rulesEngine.businessUnits,
  };
};

/** new rule definition components */
export default connect<IDispatchProp, IProps>(mapStateToProps)(RuleDefine);
