import {
  Fonts,
  FramedButton,
  FramelessButton,
  IconColor,
  IconSize,
  PrimaryIcons,
} from 'hss_components';
import Router from 'next/router';
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { IDispatchProp } from 'types/dispatch-prop';
import { FilterSectionName } from './filter-section1-details';

const PageContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  left: 24px;
`;

const TitleContainer = styled.div`
  display: flex;
  align-items: center;
  height: 35px;
`;

const TopContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const StyledChevron = styled.span`
  padding-right: 16px;
  cursor: pointer;
`;

const StyledTitle = styled(Fonts.Display32)`
  margin-bottom: 4px;
  white-space: nowrap;
`;

const ButtonContainer = styled.div`
  display: flex;
`;
const SaveButtonContainer = styled.span`
  display: flex;
  height: 35px;
`;

const StyledCancelButton = styled(FramelessButton)`
  display: flex;
  padding: 0;
  justify-content: center;
  align-items: center;
  margin-right: 16px;
  box-sizing: border-box;
  height: 35px;
  width: 85px;
  border: 1px solid ${(props) => props.theme.colors.brandBlue};
  border-radius: 2px;
  font-weight: 600;
`;

const StyledSaveButton = styled(FramedButton)`
  background-color: ${(props) => props.theme.colors.brandBlue};
  height: 35px;
  min-width: 85px;
`;

const BoldBody14Save = Fonts.makeBold(styled(Fonts.Body14)`
  color: ${(props) => props.theme.colors.white};
  margin-top: -4px;
`);

const BoldBody14 = Fonts.makeBold(Fonts.Body14);

class CreateFilterPage extends React.PureComponent<IDispatchProp> {
  backButtonCallback = () => {
    Router.push('/rules-engine');
  };
  render() {
    return (
      <PageContainer>
        <TopContainer>
          <TitleContainer>
            <StyledChevron>
              <PrimaryIcons.ChevronLeft
                size={IconSize.MEDIUM}
                color={IconColor.BLUE}
                onClick={this.backButtonCallback}
              />
            </StyledChevron>
            <StyledTitle>New Filter</StyledTitle>
          </TitleContainer>
          <ButtonContainer>
            <StyledCancelButton onClick={this.backButtonCallback}>
              <BoldBody14>Cancel </BoldBody14>
            </StyledCancelButton>
            <SaveButtonContainer
              className="inline-block"
              data-tip
              data-for="filter-save"
              style={{ cursor: 'pointer' }}
            >
              <StyledSaveButton>
                <BoldBody14Save>Save Filter</BoldBody14Save>
              </StyledSaveButton>
            </SaveButtonContainer>
          </ButtonContainer>
        </TopContainer>
        <FilterSectionName />
        <TopContainer>
          <TitleContainer />
          <ButtonContainer>
            <StyledCancelButton onClick={this.backButtonCallback}>
              <BoldBody14>Cancel </BoldBody14>
            </StyledCancelButton>
            <SaveButtonContainer
              className="inline-block"
              data-tip
              data-for="filter-save"
              style={{ cursor: 'pointer' }}
            >
              <StyledSaveButton>
                <BoldBody14Save>Save Filter</BoldBody14Save>
              </StyledSaveButton>
            </SaveButtonContainer>
          </ButtonContainer>
        </TopContainer>
      </PageContainer>
    );
  }
}

export default connect<{}, IDispatchProp, {}>(null)(CreateFilterPage);
