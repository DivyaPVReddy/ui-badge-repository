import debug from 'debug';
import puppeteer from 'puppeteer';
const log = debug('catalog');
const pageUrl = process.env.UI_TEST_PAGE_URL || 'http://localhost:8001';
const user = process.env.UI_TEST_USER || 'hscart01';
const password = process.env.UI_TEST_PASSWORD || 'Hswelcome123';

describe('Smoke Test', () => {
  let browser: puppeteer.Browser;
  let page: puppeteer.Page;

  const headless = !(process.env.HEADLESS === 'false');
  const slowMo = process.env.UI_TEST_SPEED || 0;
  const searchTerm = process.env.SEARCH_TERM || '1000';
  log(`HEADLESS env var: ${process.env.HEADLESS}`);
  log(`headless boolean set to ${headless}`);

  const login = async () => {
    browser = await puppeteer.launch({ headless, slowMo, ignoreHTTPSErrors: true });
    page = await browser.newPage();
    await page.goto(`${pageUrl}/login`);
    log('got to homepage');
    await page.type('input[name="username"]', user);
    await page.type('input[name="password"]', password);
    log('entered user/pass');
    await page.click('#submitButton');
    log('hit submit');
    await page.waitForSelector(
      '[class*="connected-catalog-search__StyledSearchBtn"]',
    );
    log('homepage is displaying');
  };

  const waitForTableToDisplayThenGetResults = async () => {
    log('waiting for table to appear');
    // Waiting for spinner twice a row confuses it;
    // confusing to users too :) Would be nice to fix in site code
    await page.waitFor(10000);
    // wait for spinner to disappear again if it's still showing
    // See: https://github.com/GoogleChrome/puppeteer/issues/705
    await page.waitFor(() => !document.querySelector('[class*="LoadingBodyText"]'), {
      timeout: 60000,
    });
    await page.waitForSelector('.rt-table', { timeout: 60000 });
    log('getting table body html');

    // need to cast to any to use outerHTML here;
    // this type is more flexible than the typescript definition thinks it is
    return page.$eval('.rt-table', elem => (elem as any).outerHTML);
  };

  const waitForLatestSearchResults = async () => {
    log('waiting for loading spinner');
    await page.waitForSelector('[class*="LoadingBodyText"]', { timeout: 60000 });
    return waitForTableToDisplayThenGetResults();
  };

  const openSearchResults = async () => {
    log('starting search method - waiting for homepage');
    // Something funny is newly happening on the homepage that is causing input not to work very briefly
    // I'm unable to replicate manually, so if it is a real issue, it goes away extremely quickly.
    await page.waitFor(2000);
    await page.waitForSelector('[class*="CatalogSearch"] input', { timeout: 60000 });
    log('found input');
    await page.type('[class*="CatalogSearch"] input', searchTerm);
    log('entered text');
    await page.click('button[class*="StyledSearch');
    log('clicked button, waiting for heading');
    return waitForTableToDisplayThenGetResults();
  };

  const switchFacilities = async () => {
    log('switching facilities');
    await page.waitForSelector('[class*="Subnav"]');
    log('subnav is present');
    await page.click('[class*="StyledAccountSelectionDropdown"]');
    log('facility selection dropdown should have opened');
    await page.waitForSelector('[class*="OptionsWrapper"]', { timeout: 60000 });
    await page.click('[class*="StyledFacilityNameBody"]'); // select the first facility
    log('clicked on first facility');
  };

  const openNextSearchPage = async () => {
    log('opening next page');
    await page.click('[class*="PageSquare"]:nth-of-type(2)');
    log('next search page clicked');
  };

  const testSearchAndPageSwitch = async () => {
    log('test starting: search and page switch');
    const originalResults = await openSearchResults();
    await switchFacilities();
    const afterSwitchResults = await waitForLatestSearchResults();
    expect(afterSwitchResults !== originalResults).toBe(true);
    await openNextSearchPage();
    const secondPageResults = await waitForLatestSearchResults();
    expect(
      secondPageResults !== afterSwitchResults &&
        secondPageResults !== originalResults,
    ).toBe(true);
    log('test complete: search and page switch');
  };

  const testOrderDetailsPage = async () => {
    log('test starting: order details page');
    log('clicking on hamburger button to open navigation sidebar');
    await page.click('[class*="StyledHamburger"]');
    log('waiting for sidebar to open');
    await page.waitForSelector('[class*="PrimaryNavigationLinks"]');

    // Need to wait for side bar to fully open; doesn't seem to work without a wait
    await page.waitFor(2000);

    log('clicking on second link (Ordering)');
    await page.click('[class*="AccordionContainer"]:nth-of-type(2)');
    log('waiting for Ordering area to expand');
    await page.waitForSelector('[class*="StyledBody1"]');
    log('clicking on first smaller link (My Orders)');
    await page.click('[class*="StyledBody1"]');
    log('waiting for my orders page to load');
    await page.waitFor(5000); // wait to make sure contents fully rendered
    log('waiting for order card to appear');
    await page.waitForSelector('[class*="NameWrapper"]');
    // relying on presence of an order to signal we're done loading
    await page.click('[class*="NameWrapper"]');
    log('clicking on first order');
    await page.click('[class*="NameWrapper"]');
    log(
      'waiting for order details page to load - using presence of account badge as key',
    );
    await page.waitForSelector('[class*="StyledAccountBadge"]');
    log('test complete: order details page');
  };

  it(
    'should be able to search and view results',
    async () => {
      try {
        await login(); // ideally would do this in beforeEach, but had trouble awaiting result
        await testSearchAndPageSwitch();
      } finally {
        browser.close(); // running async without await because we don't care about errors on close
      }
      // setting high because we'd rather the individual elements time out with clearer error messages
    },
    120000,
  );

  const openStorybook = async () => {
    browser = await puppeteer.launch({ headless, slowMo, ignoreHTTPSErrors: true });
    page = await browser.newPage();
    await page.goto(`${pageUrl}/storybook`);
    log('Storybook loaded. Checking for list of stories...');

    await page.waitForSelector('ul');
    log('List of stories is displaying on left hand side.');
  };

  it(
    'should be able to access storybook',
    async () => {
      try {
        await openStorybook();
      } finally {
        browser.close(); // running async without await because we don't care about errors on close
      }
    },
    120000,
  );

  const openBackOfficeNode = async () => {
    browser = await puppeteer.launch({ headless, slowMo, ignoreHTTPSErrors: true });
    page = await browser.newPage();
    await page.goto(
      process.env.BACK_OFFICE_NODE ||
        'https://cdhboap02.npd.amerisourcebergen.com:9002/hmc/hybris',
    );
    log('Back Office Node loaded.');

    await page.waitForSelector('input#Main_user');
    log('Login input field is present.');
  };

  it(
    'should be able to access back office node',
    async () => {
      try {
        await openBackOfficeNode();
      } finally {
        browser.close();
      }
    },
    120000,
  );

  xit(
    'should be able to view the order details page',
    async () => {
      try {
        await login();
        await testOrderDetailsPage();
      } finally {
        browser.close();
      }
    },
    120000,
  );
});
